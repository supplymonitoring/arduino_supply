﻿/**************************************************************
   WiFiManager is a library for the ESP8266/Arduino platform
   (https://github.com/esp8266/Arduino) to enable easy
   configuration and reconfiguration of WiFi credentials using a Captive Portal
   inspired by:
   http://www.esp8266.com/viewtopic.php?f=29&t=2520
   https://github.com/chriscook8/esp-arduino-apboot
   https://github.com/esp8266/Arduino/tree/master/libraries/DNSServer/examples/CaptivePortalAdvanced
   Built by AlexT https://github.com/tzapu
   Licensed under MIT license
 **************************************************************/

#include "WiFiManager.h"



WiFiManagerParameter::WiFiManagerParameter(const char *custom) {
  _id = NULL;
  _placeholder = NULL;
  _length = 0;
  _value = NULL;

  _customHTML = custom;
}

WiFiManagerParameter::WiFiManagerParameter(const char *id, const char *placeholder, const char *defaultValue, int length) {
  init(id, placeholder, defaultValue, length, "");
}

WiFiManagerParameter::WiFiManagerParameter(const char *id, const char *placeholder, const char *defaultValue, int length, const char *custom) {
  init(id, placeholder, defaultValue, length, custom);
}

void WiFiManagerParameter::init(const char *id, const char *placeholder, const char *defaultValue, int length, const char *custom) {
  _id = id;
  _placeholder = placeholder;
  _length = length;
  _value = new char[length + 1];
  for (int i = 0; i < length; i++) {
    _value[i] = 0;
  }
  if (defaultValue != NULL) {
    strncpy(_value, defaultValue, length);
  }

  _customHTML = custom;
}

const char* WiFiManagerParameter::getValue() {
  return _value;
}
const char* WiFiManagerParameter::getID() {
  return _id;
}
const char* WiFiManagerParameter::getPlaceholder() {
  return _placeholder;
}
int WiFiManagerParameter::getValueLength() {
  return _length;
}
const char* WiFiManagerParameter::getCustomHTML() {
  return _customHTML;
}

WiFiManager::WiFiManager() {
}

void WiFiManager::addParameter(WiFiManagerParameter *p) {
  if (_paramsCount + 1 > WIFI_MANAGER_MAX_PARAMS)
  {
    //Max parameters exceeded!
    //DEBUG_WM("WIFI_MANAGER_MAX_PARAMS exceeded, increase number (in WiFiManager.h) before adding more parameters!");
    //DEBUG_WM("Skipping parameter with ID:");
    //DEBUG_WM(p->getID());
    return;
  }
  _params[_paramsCount] = p;
  _paramsCount++;
  //DEBUG_WM("Adding parameter");
  //DEBUG_WM(p->getID());
}

void WiFiManager::setupConfigPortal() {
  ESP.wdtFeed();//Limpa o Watchdog timer...
  dnsServer.reset(new DNSServer());
  server.reset(new ESP8266WebServer(80));

  //DEBUG_WM(F(""));
  _configPortalStart = millis();

  //DEBUG_WM(F("Configuring access point... "));
  //DEBUG_WM(_apName);
  if (_apPassword != NULL) {
    if (strlen(_apPassword) < 8 || strlen(_apPassword) > 63) {
      // fail passphrase to short or long!
      //DEBUG_WM(F("Invalid AccessPoint password. Ignoring"));
      _apPassword = NULL;
    }
    //DEBUG_WM(_apPassword);
  }

  //optional soft ip config
  if (_ap_static_ip) {
    //DEBUG_WM(F("Custom AP IP/GW/Subnet"));
    WiFi.softAPConfig(_ap_static_ip, _ap_static_gw, _ap_static_sn);
  }

  if (_apPassword != NULL) {
    WiFi.softAP(_apName, _apPassword);//password option
  } else {
    WiFi.softAP(_apName);
  }

  delay(500); // Without delay I've seen the IP address blank
  //DEBUG_WM(F("AP IP address: "));
  //DEBUG_WM(WiFi.softAPIP());

  /* Setup the DNS server redirecting all the domains to the apIP */
  dnsServer->setErrorReplyCode(DNSReplyCode::NoError);
  dnsServer->start(DNS_PORT, "*", WiFi.softAPIP());

  /* Setup web pages: root, wifi config pages, SO captive portal detectors and not found. */
  server->on("/", std::bind(&WiFiManager::handleRoot, this));
  server->on("/wifi", std::bind(&WiFiManager::handleWifi, this, true));
  //server->on("/0wifi", std::bind(&WiFiManager::handleWifi, this, false));
  server->on("/wifisave", std::bind(&WiFiManager::handleWifiSave, this));
  server->on("/wifi-delete", std::bind(&WiFiManager::handleWifiDelete, this));
  server->on("/ajustes", std::bind(&WiFiManager::handleAjustes, this));
  server->on("/sensores", std::bind(&WiFiManager::handleSensores, this));
  server->on("/tcs", std::bind(&WiFiManager::handleTCS, this));  
  server->on("/em-settings", std::bind(&WiFiManager::handleAjustesSave, this));  
  server->on("/i", std::bind(&WiFiManager::handleInfo, this));
  server->on("/r", std::bind(&WiFiManager::handleReset, this));
  //server->on("/generate_204", std::bind(&WiFiManager::handle204, this));  //Android/Chrome OS captive portal check.
  server->on("/fwlink", std::bind(&WiFiManager::handleRoot, this));  //Microsoft captive portal. Maybe not needed. Might be handled by notFound handler.
  server->onNotFound (std::bind(&WiFiManager::handleNotFound, this));
  server->begin(); // Web server start
  //DEBUG_WM(F("HTTP server started"));

}

boolean WiFiManager::autoConnect() {
  ESP.wdtFeed();//Limpa o Watchdog timer...
  String ssid = "ESP" + String(ESP.getChipId());
  return autoConnect(ssid.c_str(), NULL);
}

boolean WiFiManager::autoConnect(char const *apName, char const *apPassword) {
  ESP.wdtFeed();//Limpa o Watchdog timer...
  //DEBUG_WM(F(""));
  //DEBUG_WM(F("AutoConnect"));

  // read eeprom for ssid and pass
  //String ssid = getSSID();
  //String pass = getPassword();

  // attempt to connect; should it fail, fall back to AP
  WiFi.mode(WIFI_STA);

  if (connectWifi("", "") == WL_CONNECTED)   {
    //DEBUG_WM(F("IP Address:"));
    //DEBUG_WM(WiFi.localIP());
    //connected
    return true;
  }

  return startConfigPortal(apName, apPassword);
}

boolean WiFiManager::configPortalHasTimeout() {
  ESP.wdtFeed();//Limpa o Watchdog timer...
  if (_configPortalTimeout == 0 || wifi_softap_get_station_num() > 0) {
    _configPortalStart = millis(); // kludge, bump configportal start time to skew timeouts
    return false;
  }
  return (millis() > _configPortalStart + _configPortalTimeout);
}

boolean WiFiManager::startConfigPortal() {
  String ssid = "ESP" + String(ESP.getChipId());
  return startConfigPortal(ssid.c_str(), NULL);
}

boolean  WiFiManager::startConfigPortal(char const *apName, char const *apPassword) {
  //setup AP
  WiFi.mode(WIFI_AP_STA);
  //DEBUG_WM("SET AP STA");

  _apName = apName;
  _apPassword = apPassword;

  //notify we entered AP mode
  if ( _apcallback != NULL) {
    _apcallback(this);
  }

  connect = false;
  setupConfigPortal();

  while (1) {

    // check if timeout
    if (configPortalHasTimeout()) break;

    //DNS
    dnsServer->processNextRequest();
    //HTTP
    server->handleClient();


    if (connect) {
      connect = false;
      delay(2000);
      //DEBUG_WM(F("Connecting to new AP"));

      // using user-provided  _ssid, _pass in place of system-stored ssid and pass
      if (connectWifi(_ssid, _pass) != WL_CONNECTED) {
        //DEBUG_WM(F("Failed to connect."));
      } else {
        //connected
        WiFi.mode(WIFI_STA);
        //notify that configuration has changed and any optional parameters should be saved
        if ( _savecallback != NULL) {
          //todo: check if any custom parameters actually exist, and check if they really changed maybe
          _savecallback();
        }
        break;
      }

      if (_shouldBreakAfterConfig) {
        //flag set to exit after config after trying to connect
        //notify that configuration has changed and any optional parameters should be saved
        if ( _savecallback != NULL) {
          //todo: check if any custom parameters actually exist, and check if they really changed maybe
          _savecallback();
        }
        break;
      }
    }
    yield();
  }

  server.reset();
  dnsServer.reset();

  return  WiFi.status() == WL_CONNECTED;
}


int WiFiManager::connectWifi(String ssid, String pass) {
  //DEBUG_WM(F("Connecting as wifi client..."));

  // check if we've got static_ip settings, if we do, use those.
  if (_sta_static_ip) {
    //DEBUG_WM(F("Custom STA IP/GW/Subnet"));
    WiFi.config(_sta_static_ip, _sta_static_gw, _sta_static_sn);
    //DEBUG_WM(WiFi.localIP());
  }
  //fix for auto connect racing issue
  if (WiFi.status() == WL_CONNECTED) {
    //DEBUG_WM("Already connected. Bailing out.");
    return WL_CONNECTED;
  }
  //check if we have ssid and pass and force those, if not, try with last saved values
  if (ssid != "") {
    WiFi.begin(ssid.c_str(), pass.c_str());
  } else {
    if (WiFi.SSID()) {
      //DEBUG_WM("Using last saved values, should be faster");
      //trying to fix connection in progress hanging
      ETS_UART_INTR_DISABLE();
      wifi_station_disconnect();
      ETS_UART_INTR_ENABLE();

      WiFi.begin();
    } else {
      //DEBUG_WM("No saved credentials");
    }
  }

  int connRes = waitForConnectResult();
  //DEBUG_WM ("Connection result: ");
  //DEBUG_WM ( connRes );
  //not connected, WPS enabled, no pass - first attempt
  if (_tryWPS && connRes != WL_CONNECTED && pass == "") {
    startWPS();
    //should be connected at the end of WPS
    connRes = waitForConnectResult();
  }
  return connRes;
}

uint8_t WiFiManager::waitForConnectResult() {
  if (_connectTimeout == 0) {
    return WiFi.waitForConnectResult();
  } else {
    //DEBUG_WM (F("Waiting for connection result with time out"));
    unsigned long start = millis();
    boolean keepConnecting = true;
    uint8_t status;
    while (keepConnecting) {
      status = WiFi.status();
      if (millis() > start + _connectTimeout) {
        keepConnecting = false;
        //DEBUG_WM (F("Connection timed out"));
      }
      if (status == WL_CONNECTED || status == WL_CONNECT_FAILED) {
        keepConnecting = false;
      }
      delay(100);
    }
    return status;
  }
}

void WiFiManager::startWPS() {
  //DEBUG_WM("START WPS");
  WiFi.beginWPSConfig();
  //DEBUG_WM("END WPS");
}
/*
  String WiFiManager::getSSID() {
  if (_ssid == "") {
    //DEBUG_WM(F("Reading SSID"));
    _ssid = WiFi.SSID();
    //DEBUG_WM(F("SSID: "));
    //DEBUG_WM(_ssid);
  }
  return _ssid;
  }

  String WiFiManager::getPassword() {
  if (_pass == "") {
    //DEBUG_WM(F("Reading Password"));
    _pass = WiFi.psk();
    //DEBUG_WM("Password: " + _pass);
    ////DEBUG_WM(_pass);
  }
  return _pass;
  }
*/
String WiFiManager::getConfigPortalSSID() {
  return _apName;
}

void WiFiManager::resetSettings() {
  //DEBUG_WM(F("settings invalidated"));
  //DEBUG_WM(F("THIS MAY CAUSE AP NOT TO START UP PROPERLY. YOU NEED TO COMMENT IT OUT AFTER ERASING THE DATA."));
  ESP.wdtFeed();//Limpa o Watchdog timer...
  WiFi.disconnect(true);
  //delay(200);
}

void WiFiManager::setTimeout(unsigned long seconds) {
  setConfigPortalTimeout(seconds);
}

void WiFiManager::setConfigPortalTimeout(unsigned long seconds) {
  _configPortalTimeout = seconds * 1000;
}

void WiFiManager::setConnectTimeout(unsigned long seconds) {
  _connectTimeout = seconds * 1000;
}

void WiFiManager::setDebugOutput(boolean debug) {
  _debug = debug;
}

void WiFiManager::setAPStaticIPConfig(IPAddress ip, IPAddress gw, IPAddress sn) {
  _ap_static_ip = ip;
  _ap_static_gw = gw;
  _ap_static_sn = sn;
}

void WiFiManager::setSTAStaticIPConfig(IPAddress ip, IPAddress gw, IPAddress sn) {
  _sta_static_ip = ip;
  _sta_static_gw = gw;
  _sta_static_sn = sn;
}

void WiFiManager::setMinimumSignalQuality(int quality) {
  _minimumQuality = quality;
}

void WiFiManager::setBreakAfterConfig(boolean shouldBreak) {
  _shouldBreakAfterConfig = shouldBreak;
}

/** Handle root or redirect to captive portal */
void WiFiManager::handleRoot() {
  //DEBUG_WM(F("Handle root"));
  if (captivePortal()) { // If caprive portal redirect instead of displaying the page.
    return;
  }

  String page = FPSTR(HTTP_HEAD);
  page.replace("{v}", ".:Supply Monitoring:.");
  page += FPSTR(HTTP_SCRIPT);
  page += FPSTR(HTTP_STYLE);
  page += _customHeadElement;
  page += FPSTR(HTTP_HEAD_END);
  page += "<h1>";
  page += _apName;
  page += "</h1>";
  page += F("<h3>Supply Monitoring</h3>");
  page += F("<h3>Assistente de Configuracao</h3>");
  page += FPSTR(HTTP_PORTAL_OPTIONS);
  page += FPSTR(HTTP_END);

  server->sendHeader("Content-Length", String(page.length()));
  server->send(200, "text/html", page);

}

/** Wifi config page handler */
void WiFiManager::handleWifi(boolean scan) {

  String page = FPSTR(HTTP_HEAD);
  page.replace("{v}", ".:Buscar redes Wi-Fi:.");
  page += FPSTR(HTTP_SCRIPT);
  page += FPSTR(HTTP_STYLE);
  page += _customHeadElement;
  page += FPSTR(HTTP_HEAD_END);

  if (scan) {
    int n = WiFi.scanNetworks();
    //DEBUG_WM(F("Scan done"));
    if (n == 0) {
      //DEBUG_WM(F("No networks found"));
      page += F("Nenhuma rede encontrada. Por favor, tente novamente.");
    }
    else {

      //sort networks
      int indices[n];
      for (int i = 0; i < n; i++) {
        indices[i] = i;
      }

      // RSSI SORT

      // old sort
      for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
          if (WiFi.RSSI(indices[j]) > WiFi.RSSI(indices[i])) {
            std::swap(indices[i], indices[j]);
          }
        }
      }

      /*std::sort(indices, indices + n, [](const int & a, const int & b) -> bool
        {
        return WiFi.RSSI(a) > WiFi.RSSI(b);
        });*/

      // remove duplicates ( must be RSSI sorted )
      if (_removeDuplicateAPs) {
        String cssid;
        for (int i = 0; i < n; i++) {
          if (indices[i] == -1) continue;
          cssid = WiFi.SSID(indices[i]);
          for (int j = i + 1; j < n; j++) {
            if (cssid == WiFi.SSID(indices[j])) {
              //DEBUG_WM("DUP AP: " + WiFi.SSID(indices[j]));
              indices[j] = -1; // set dup aps to index -1
            }
          }
        }
      }

      //display networks in page
      for (int i = 0; i < n; i++) {
        if (indices[i] == -1) continue; // skip dups
        //DEBUG_WM(WiFi.SSID(indices[i]));
        //DEBUG_WM(WiFi.RSSI(indices[i]));
        int quality = getRSSIasQuality(WiFi.RSSI(indices[i]));

        if (_minimumQuality == -1 || _minimumQuality < quality) {
          String item = FPSTR(HTTP_ITEM);
          String rssiQ;
          rssiQ += quality;
          item.replace("{v}", WiFi.SSID(indices[i]));
          item.replace("{r}", rssiQ);
          if (WiFi.encryptionType(indices[i]) != ENC_TYPE_NONE) {
            item.replace("{i}", "l");
          } else {
            item.replace("{i}", "");
          }
          ////DEBUG_WM(item);
          page += item;
          delay(0);
        } else {
          //DEBUG_WM(F("Skipping due to quality"));
        }

      }
      page += "<br/>";
    }
  }

  page += FPSTR(HTTP_FORM_START);
  char parLength[5];
  // add the extra parameters to the form
  for (int i = 0; i < _paramsCount; i++) {
    if (_params[i] == NULL) {
      break;
    }

    String pitem = FPSTR(HTTP_FORM_PARAM);
    if (_params[i]->getID() != NULL) {
      pitem.replace("{i}", _params[i]->getID());
      pitem.replace("{n}", _params[i]->getID());
      pitem.replace("{p}", _params[i]->getPlaceholder());
      snprintf(parLength, 5, "%d", _params[i]->getValueLength());
      pitem.replace("{l}", parLength);
      pitem.replace("{v}", _params[i]->getValue());
      pitem.replace("{c}", _params[i]->getCustomHTML());
    } else {
      pitem = _params[i]->getCustomHTML();
    }

    page += pitem;
  }
  if (_params[0] != NULL) {
    page += "<br/>";
  }

  if (_sta_static_ip) {

    String item = FPSTR(HTTP_FORM_PARAM);
    item.replace("{i}", "ip");
    item.replace("{n}", "ip");
    item.replace("{p}", "Static IP");
    item.replace("{l}", "15");
    item.replace("{v}", _sta_static_ip.toString());

    page += item;

    item = FPSTR(HTTP_FORM_PARAM);
    item.replace("{i}", "gw");
    item.replace("{n}", "gw");
    item.replace("{p}", "Static Gateway");
    item.replace("{l}", "15");
    item.replace("{v}", _sta_static_gw.toString());

    page += item;

    item = FPSTR(HTTP_FORM_PARAM);
    item.replace("{i}", "sn");
    item.replace("{n}", "sn");
    item.replace("{p}", "Subnet");
    item.replace("{l}", "15");
    item.replace("{v}", _sta_static_sn.toString());

    page += item;

    page += "<br/>";
  }

  page += FPSTR(HTTP_FORM_END);
  page += FPSTR(HTTP_SCAN_LINK);

  page += FPSTR(HTTP_END);

  server->sendHeader("Content-Length", String(page.length()));
  server->send(200, "text/html", page);


  //DEBUG_WM(F("Sent config page"));
}

/** Handle the WLAN save form and redirect to WLAN config page again */
void WiFiManager::handleWifiSave() {
  //DEBUG_WM(F("WiFi save"));

  //SAVE/connect here
  _ssid = server->arg("s").c_str();
  _pass = server->arg("p").c_str();
  _pass = server->arg("p").c_str();

  //parameters
  for (int i = 0; i < _paramsCount; i++) {
    if (_params[i] == NULL) {
      break;
    }
    //read parameter
    String value = server->arg(_params[i]->getID()).c_str();
    //store it in array
    value.toCharArray(_params[i]->_value, _params[i]->_length);
    //DEBUG_WM(F("Parameter"));
    //DEBUG_WM(_params[i]->getID());
    //DEBUG_WM(value);
  }

  if (server->arg("ip") != "") {
    //DEBUG_WM(F("static ip"));
    //DEBUG_WM(server->arg("ip"));
    //_sta_static_ip.fromString(server->arg("ip"));
    String ip = server->arg("ip");
    optionalIPFromString(&_sta_static_ip, ip.c_str());
  }
  if (server->arg("gw") != "") {
    //DEBUG_WM(F("static gateway"));
    //DEBUG_WM(server->arg("gw"));
    String gw = server->arg("gw");
    optionalIPFromString(&_sta_static_gw, gw.c_str());
  }
  if (server->arg("sn") != "") {
    //DEBUG_WM(F("static netmask"));
    //DEBUG_WM(server->arg("sn"));
    String sn = server->arg("sn");
    optionalIPFromString(&_sta_static_sn, sn.c_str());
  }

  String page = FPSTR(HTTP_HEAD);
  page.replace("{v}", "Rede configurada");
  page += FPSTR(HTTP_SCRIPT);
  page += FPSTR(HTTP_STYLE);
  page += _customHeadElement;
  page += FPSTR(HTTP_HEAD_END);
  page += FPSTR(HTTP_SAVED);
  page += FPSTR(HTTP_END);

  server->sendHeader("Content-Length", String(page.length()));
  server->send(200, "text/html", page);

  //DEBUG_WM(F("Sent wifi save page"));

  connect = true; //signal ready to connect/reset
}

String WiFiManager::getCelClientes() {

  _celClientes = "";
  _celClientes += _celCliente1;
  _celClientes += ";";
  _celClientes += _celCliente2;
  _celClientes += ";";
  _celClientes += _celCliente3;
  _celClientes += ";";
  return _celClientes;
}

void WiFiManager::setCelClientes(String cel1, String cel2, String cel3) {

  _celCliente1 = cel1;
  _celCliente2 = cel2;
  _celCliente3 = cel3;
}

void WiFiManager::setSIParams(String siTempMin, String siTempMax, String siTemp_Atual_Aferida,String ktSI, String siUmidMin, String siUmidMax, String siUmid_Atual_Aferida, String kuSI) {

  _tempSIMin = siTempMin;
  _tempSIMax = siTempMax;

  _tempSIAtual = siTemp_Atual_Aferida;
  _ktSI = ktSI;
  
  if (_tempSIAtual.toFloat() > 900) {
    _tempSIAtual = "-";
  }
  _umidSIMin = siUmidMin;
  _umidSIMax = siUmidMax;

  _umidSIAtual = siUmid_Atual_Aferida;
  _kuSI = kuSI;

  if (_umidSIAtual.toFloat() > 900 ) {
    _umidSIAtual = "-";
  }

}

String WiFiManager::getSIParams() {
  //Formato do retorno:
  //temp_min_si;temp_max_si;si_temp_atual;kt_si;umid_min_si;umid_max_si;si_umid_atual;ku_si;
  //17.0;26.0;999.0;1.0;25.0;70.0;999.0;1.0

  String siParamsRetorno = "";
  if (_tempSIAtual.indexOf("-") >= 0) {
	  _tempSIAtual = 999;
  }
  
  if (_umidSIAtual.indexOf("-") >= 0) {
	  _umidSIAtual = 999;
  }

  siParamsRetorno += _tempSIMin;
  siParamsRetorno += ";";
  siParamsRetorno += _tempSIMax;
  siParamsRetorno += ";";
  siParamsRetorno += _tempSIAtual;
  siParamsRetorno += ";";
  siParamsRetorno += _ktSI;
  siParamsRetorno += ";";
  siParamsRetorno += _umidSIMin;
  siParamsRetorno += ";";
  siParamsRetorno += _umidSIMax;
  siParamsRetorno += ";";
  siParamsRetorno += _umidSIAtual;
  siParamsRetorno += ";";
  siParamsRetorno += _kuSI;
  siParamsRetorno += ";";  

  return siParamsRetorno;
}

void WiFiManager::setSEParams(String temp_min_s1, String temp_max_s1, String temp_atual_se1,String temp_min_s2, String temp_max_s2, String temp_atual_se2) {
  //temp_min_s1;temp_max_s1;kt_s1;temp_min_s2;temp_max_s2;kt_s2
  
  if (temp_atual_se1.toFloat() > 900) {
    temp_atual_se1 = "-";
  }
  if (temp_atual_se2.toFloat() > 900) {
    temp_atual_se2 = "-";
  }
  
  _tempSE1Min = temp_min_s1;
  _tempSE1Max = temp_max_s1;
  _tempSE1_Atual = temp_atual_se1;
  _tempSE2Min = temp_min_s2;
  _tempSE2Max = temp_max_s2;
  _tempSE2_Atual = temp_atual_se2;

}

String WiFiManager::getSEParams() {
  //Formato do retorno:
  //temp_min_s1;temp_max_s1;kt_s1;temp_min_s2;temp_max_s2;kt_s2
  //6.0;16.0;999;10.0;20.0;999

  String siParamsRetorno = "";
  
  if (_tempSE1_Atual.indexOf("-") >= 0) {
	  _tempSE1_Atual = 999;
  }
  
  if (_tempSE2_Atual.indexOf("-") >= 0) {
	  _tempSE2_Atual = 999;
  }

  siParamsRetorno += _tempSE1Min;
  siParamsRetorno += ";";
  siParamsRetorno += _tempSE1Max;
  siParamsRetorno += ";";
  siParamsRetorno += _tempSE1_Atual;
  siParamsRetorno += ";";
  siParamsRetorno += _tempSE2Min;
  siParamsRetorno += ";";
  siParamsRetorno += _tempSE2Max;
  siParamsRetorno += ";";
  siParamsRetorno += _tempSE2_Atual;
  siParamsRetorno += ";";

  return siParamsRetorno;


}

void WiFiManager::setTCParams(String faseR_cMin, String faseR_cMax, String faseR_cal_aferida, String faseS_cMin, String faseS_cMax, String faseS_cal_aferida, String faseT_cMin, String faseT_cMax, String faseT_cal_aferida) {
  //faseR_cMin;faseR_cMax;faseR_k;faseS_cMin;faseS_cMax;faseS_k;faseT_cMin;faseT_cMax;faseT_k;
  _faseRmin = faseR_cMin;
  _faseRmax = faseR_cMax;
  _faseR_calaferida = "-";
  //_faseR_calaferida = "-1.0";

  _faseSmin = faseS_cMin;
  _faseSmax = faseS_cMax;
  _faseS_calaferida = "-";
  //_faseS_calaferida = "-1.0";

  _faseTmin = faseT_cMin;
  _faseTmax = faseT_cMax;
  _faseT_calaferida = "-";
  //_faseT_calaferida = "-1.0";
}

String WiFiManager::getTCParams() {
  //Formato do retorno:
  //faseR_min;faseR_max;faseR_atual(calcular o faseR_Cal_Aferidac);faseS_min;faseS_max;faseS_atual(calcular o faseS_Cal_Aferidac);faseT_min;faseT_max;faseT_atual(calcular o faseT_Cal_Aferidac);
  //1.0;50.0;1.0;1.0;50.0;1.0;1.0;50.0;1.0

  String tcParamsRetorno = "";
  
  if(_faseR_calaferida.indexOf("-") >= 0){
	  _faseR_calaferida = 999;
  }
  if(_faseS_calaferida.indexOf("-") >= 0){
	  _faseS_calaferida = 999;
  }
  if(_faseT_calaferida.indexOf("-") >= 0){  
	  _faseT_calaferida = 999;
  }  

  tcParamsRetorno += _faseRmin;
  tcParamsRetorno += ";";
  tcParamsRetorno += _faseRmax;
  tcParamsRetorno += ";";
  tcParamsRetorno += _faseR_calaferida;
  tcParamsRetorno += ";";
  tcParamsRetorno += _faseSmin;
  tcParamsRetorno += ";";
  tcParamsRetorno += _faseSmax;
  tcParamsRetorno += ";";
  tcParamsRetorno += _faseS_calaferida;
  tcParamsRetorno += ";";
  tcParamsRetorno += _faseTmin;
  tcParamsRetorno += ";";
  tcParamsRetorno += _faseTmax;
  tcParamsRetorno += ";";
  tcParamsRetorno += _faseT_calaferida;
  tcParamsRetorno += ";";

  return tcParamsRetorno;
}

/** Handle the Settings of EM*/
void WiFiManager::handleAjustes() {
  //DEBUG_WM(F("EM Settings"));
  String page = FPSTR(HTTP_HEAD);

  page.replace("{v}", ".:Ajustes de Preferencias:.");

  page += F("<b>");
  page += F("<dt>Ajustes de Preferencias</dt><dd>");
  page += F("</b>");
  page += FPSTR(HTTP_SCRIPT);
  page += FPSTR(HTTP_STYLE);
  page += _customHeadElement;
  page += FPSTR(HTTP_HEAD_END);
  
  //CELULARES
  page += F("<dl>");
  page += F("<dt>Celulares do cliente que receberao notificação / SMS:</dt><dd>");
  page += F("</dl>");
  page += FPSTR(HTTP_FORM_CONFIG_PHONES);

  page.replace("Cel Cliente 01: ", _celCliente1);
  page.replace("Cel Cliente 02: ", _celCliente2);
  page.replace("Cel Cliente 03: ", _celCliente3);
      
  page += FPSTR(HTTP_FORM_BACK);
  page += FPSTR(HTTP_FORM_END);
  page += FPSTR(HTTP_END);

  server->sendHeader("Content-Length", String(page.length()));
  server->send(200, "text/html", page);

  //DEBUG_WM(F("Sent wifi save page"));

}

/** Handle the Settings of EM*/
void WiFiManager::handleSensores() {
  //DEBUG_WM(F("EM Settings"));
  String page = FPSTR(HTTP_HEAD);

  page.replace("{v}", ".:Ajustes de Sensores:.");

  page += F("<b>");
  page += F("<dt>Ajustes de Sensores</dt><dd>");
  page += F("</b>");
  page += FPSTR(HTTP_SCRIPT);
  page += FPSTR(HTTP_STYLE);
  page += _customHeadElement;
  page += FPSTR(HTTP_HEAD_END);
    
  //SENSOR TEMP/UMID INTERNO
  page += F("<dl>");
  page += F("<dt>SI - TU:</dt><dd>");
  page += F("</dl>");
  page += FPSTR(HTTP_FORM_CONFIG_SI);
  page.replace("SI - Temp Min", _tempSIMin);
  page.replace("SI - Temp Max", _tempSIMax);
  page.replace("SI - Temp Atual", _tempSIAtual);
  page.replace("SI - Umid Min", _umidSIMin);
  page.replace("SI - Umid Max", _umidSIMax);
  page.replace("SI - Umid Atual", _umidSIAtual);
  
  //SENSOR TEMP EXTERNO 1
  page += F("<dl>");
  page += F("<dt>SE1 - T:</dt><dd>");
  page += F("</dl>");
  page += FPSTR(HTTP_FORM_CONFIG_SE1);
  page.replace("SE1 - Temp Min", _tempSE1Min);
  page.replace("SE1 - Temp Max", _tempSE1Max);
  page.replace("SE1 - Temp Atual", _tempSE1_Atual);
  
  //SENSOR TEMP EXTERNO 2
  page += F("<dl>");
  page += F("<dt>SE2 - T:</dt><dd>");
  page += F("</dl>");
  page += FPSTR(HTTP_FORM_CONFIG_SE2);
  page.replace("SE2 - Temp Min", _tempSE2Min);
  page.replace("SE2 - Temp Max", _tempSE2Max);
  page.replace("SE2 - Temp Atual", _tempSE2_Atual);

  page += FPSTR(HTTP_FORM_BACK);
  page += FPSTR(HTTP_FORM_END);
  page += FPSTR(HTTP_END);

  server->sendHeader("Content-Length", String(page.length()));
  server->send(200, "text/html", page);

  //DEBUG_WM(F("Sent wifi save page"));

}

/** Handle the Settings of EM*/
void WiFiManager::handleTCS() {
  //DEBUG_WM(F("EM Settings"));
  String page = FPSTR(HTTP_HEAD);

  page.replace("{v}", ".:Ajustes dos TC's:.");

  page += F("<b>");
  page += F("<dt>Ajustes dos TC's</dt><dd>");
  page += F("</b>");
  page += FPSTR(HTTP_SCRIPT);
  page += FPSTR(HTTP_STYLE);
  page += _customHeadElement;
  page += FPSTR(HTTP_HEAD_END);
     
  //SENSORES DE CORRENTE - TCS
  page += F("<dl>");
  page += F("<dt>TC's:</dt><dd>");
  page += F("</dl>");
  page += FPSTR(HTTP_FORM_CONFIG_TC1);
  page.replace("TC1 Min", _faseRmin);
  page.replace("TC1 Max", _faseRmax);
  page.replace("TC1 Atual", _faseR_calaferida);

  page += FPSTR(HTTP_FORM_CONFIG_TC2);
  page.replace("TC2 Min", _faseSmin);
  page.replace("TC2 Max", _faseSmax);
  page.replace("TC2 Atual", _faseS_calaferida);

  page += FPSTR(HTTP_FORM_CONFIG_TC3);
  page.replace("TC3 Min", _faseTmin);
  page.replace("TC3 Max", _faseTmax);
  page.replace("TC3 Atual", _faseT_calaferida);
  
  page += FPSTR(HTTP_FORM_BACK);
  page += FPSTR(HTTP_FORM_END);
  page += FPSTR(HTTP_END);

  server->sendHeader("Content-Length", String(page.length()));
  server->send(200, "text/html", page);

  //DEBUG_WM(F("Sent wifi save page"));

}

/** Handle the saved settings of EM*/
void WiFiManager::handleAjustesSave() {

  String strAux = "";

  //SALVANDO ALTERACOES
  
  //CELULARES CLIENTE
  if (server->arg("cel_cliente1") != "" ) {
    strAux = server->arg("cel_cliente1").c_str();
    if (strAux.indexOf(_celCliente1) < 0) {	//celular digitado eh diferente do celCliente anterior
      _celCliente1 = strAux;
    }
  }

  if (server->arg("cel_cliente2") != "" ) {
    strAux = server->arg("cel_cliente2").c_str();
    if (strAux.indexOf(_celCliente2) < 0) {	//celular digitado eh diferente do celCliente anterior
      _celCliente2 = strAux;
    }
  }

  if (server->arg("cel_cliente3") != "" ) {
    strAux = server->arg("cel_cliente3").c_str();
    if (strAux.indexOf(_celCliente3) < 0) {	//celular digitado eh diferente do celCliente anterior
      _celCliente3 = strAux;
    }
  }
  
  //SENSOR TEMP/UMID INTERNO
  //TEMP MINIMA
  if (server->arg("temp_min_si") != "" ) {
    strAux = server->arg("temp_min_si").c_str();
    if (strAux.indexOf(_tempSIMin) < 0) {	//temperatura minima SI digitado eh diferente do _tempSIMin anterior
      _tempSIMin = strAux;
    }
  }

  //TEMP MAXIMA
  if (server->arg("temp_max_si") != "" ) {
    strAux = server->arg("temp_max_si").c_str();
    if (strAux.indexOf(_tempSIMax) < 0) {	//temperatura maxima SI digitado eh diferente do _tempSIMax anterior
      _tempSIMax = strAux;
    }
  }

  //TEMP ATUAL
  if (server->arg("temp_atual_si") != "" ) {
    strAux = server->arg("temp_atual_si").c_str();
    if (strAux.indexOf(_tempSIAtual) < 0) {	//temperatura atual SI digitado eh diferente do _tempSIAtual anterior
      _tempSIAtual = strAux;
    }
  }

  //UMID MINIMA
  if (server->arg("umid_min_si") != "" ) {
    strAux = server->arg("umid_min_si").c_str();
    if (strAux.indexOf(_umidSIMin) < 0) {	//umidade minima SI digitado eh diferente do _umidSIMin anterior
      _umidSIMin = strAux;
    }
  }

  //UMID MINIMA
  if (server->arg("umid_max_si") != "" ) {
    strAux = server->arg("umid_max_si").c_str();
    if (strAux.indexOf(_umidSIMax) < 0) {	//umidade maxima SI digitado eh diferente do _umidSIMax anterior
      _umidSIMax = strAux;
    }
  }

  //UMID ATUAL
  if (server->arg("umid_atual_si") != "" ) {
    strAux = server->arg("umid_atual_si").c_str();
    if (strAux.indexOf(_umidSIAtual) < 0) {	//umidade atual SI digitado eh diferente do _umidSIAtual anterior
      _umidSIAtual = strAux;
    }
  }

  //SENSOR TEMP EXTERNO 1
  //TEMP MINIMA
  if (server->arg("temp_min_se1") != "" ) {
    strAux = server->arg("temp_min_se1").c_str();
    if (strAux.indexOf(_tempSE1Min) < 0) {	//temperatura minima SE1 digitado eh diferente do _tempSE1Min anterior
      _tempSE1Min = strAux;
    }
  }

  //TEMP MAXIMA
  if (server->arg("temp_max_se1") != "" ) {
    strAux = server->arg("temp_max_se1").c_str();
    if (strAux.indexOf(_tempSE1Max) < 0) {	//temperatura maxima SE1 digitado eh diferente do _tempSIMax anterior
      _tempSE1Max = strAux;
    }
  }

  //TEMP CALIBRACAO
  if (server->arg("temp_se1_cal_aferida") != "" ) {
    strAux = server->arg("temp_se1_cal_aferida").c_str();
    if (strAux.indexOf(_tempSE1_Atual) < 0) {	//temperatura de calibracao SE1 digitado eh diferente do _tempSE1_Atual anterior
      _tempSE1_Atual = strAux;
    }
  }

  //SENSOR TEMP EXTERNO 2
  //TEMP MINIMA
  if (server->arg("temp_min_se2") != "" ) {
    strAux = server->arg("temp_min_se2").c_str();
    if (strAux.indexOf(_tempSE2Min) < 0) {	//temperatura minima SE2 digitado eh diferente do _tempSE2Min anterior
      _tempSE2Min = strAux;
    }
  }

  //TEMP MAXIMA
  if (server->arg("temp_max_se2") != "" ) {
    strAux = server->arg("temp_max_se2").c_str();
    if (strAux.indexOf(_tempSE2Max) < 0) {	//temperatura maxima SE2 digitado eh diferente do _tempSE2Max anterior
      _tempSE2Max = strAux;
    }
  }

  //TEMP CALIBRACAO
  if (server->arg("temp_se2_cal_aferida") != "" ) {
    strAux = server->arg("temp_se2_cal_aferida").c_str();
    if (strAux.indexOf(_tempSE2_Atual) < 0) {	//temperatura de calibracao SE2 digitado eh diferente do _tempSE2_Atual anterior
      _tempSE2_Atual = strAux;
    }
  }

  //SENSORES DE CORRENTE - TCS
  //TC1 - Min
  if (server->arg("fase_R_Min") != "" ) {
    strAux = server->arg("fase_R_Min").c_str();
    if (strAux.indexOf(_faseRmin) < 0) {	//corrente minima TC1 digitado eh diferente do _faseRmin anterior
      _faseRmin = strAux;
    }
  }

  //TC1 - Max
  if (server->arg("fase_R_Max") != "" ) {
    strAux = server->arg("fase_R_Max").c_str();
    if (strAux.indexOf(_faseRmax) < 0) {	//corrente minima TC1 digitado eh diferente do _faseRmax anterior
      _faseRmax = strAux;
    }
  }

  //TC1 - Corrente Calibracao
  if (server->arg("fase_R_Atual") != "" ) {
    strAux = server->arg("fase_R_Atual").c_str();
    if (strAux.indexOf(_faseR_calaferida) < 0) {	//corrente aferida (calibracao) TC1 digitado eh diferente do _faseR_calaferida anterior
      _faseR_calaferida = strAux;
    }
  }


  //TC2 - Min
  if (server->arg("fase_S_Min") != "" ) {
    strAux = server->arg("fase_S_Min").c_str();
    if (strAux.indexOf(_faseSmin) < 0) {	//corrente minima TC2 digitado eh diferente do _faseSmin anterior
      _faseSmin = strAux;
    }
  }

  //TC2 - Max
  if (server->arg("fase_S_Max") != "" ) {
    strAux = server->arg("fase_S_Max").c_str();
    if (strAux.indexOf(_faseSmax) < 0) {	//corrente minima TC1 digitado eh diferente do _faseSmax anterior
      _faseSmax = strAux;
    }
  }

  //TC2 - Corrente Calibracao
  if (server->arg("fase_S_Atual") != "" ) {
    strAux = server->arg("fase_S_Atual").c_str();
    if (strAux.indexOf(_faseS_calaferida) < 0) {	//corrente aferida (calibracao) TC2 digitado eh diferente do _faseS_calaferida anterior
      _faseS_calaferida = strAux;
    }
  }

  //TC3 - Min
  if (server->arg("fase_T_Min") != "" ) {
    strAux = server->arg("fase_T_Min").c_str();
    if (strAux.indexOf(_faseTmin) < 0) {	//corrente minima TC3 digitado eh diferente do _faseTmin anterior
      _faseTmin = strAux;
    }
  }

  //TC3 - Max
  if (server->arg("fase_T_Max") != "" ) {
    strAux = server->arg("fase_T_Max").c_str();
    if (strAux.indexOf(_faseTmax) < 0) {	//corrente minima TC3 digitado eh diferente do _faseTmax anterior
      _faseTmax = strAux;
    }
  }

  //TC3 - Corrente Calibracao
  if (server->arg("fase_T_Atual") != "" ) {
    strAux = server->arg("fase_T_Atual").c_str();
    if (strAux.indexOf(_faseT_calaferida) < 0) {	//corrente aferida (calibracao) TC3 digitado eh diferente do _faseT_calaferida anterior
      _faseT_calaferida = strAux;
    }
  }

  String page = FPSTR(HTTP_HEAD);
  page.replace("{v}", "Ajustes salvos!");
  page += FPSTR(HTTP_SCRIPT);
  page += FPSTR(HTTP_STYLE);
  page += _customHeadElement;
  page += FPSTR(HTTP_HEAD_END);
  page += FPSTR(HTTP_SAVED2);
  page += FPSTR(HTTP_FORM_BACK);
  page += FPSTR(HTTP_END);
  delay(2000);
  handleRoot();
  server->sendHeader("Content-Length", String(page.length()));
  server->send(200, "text/html", page);

}


/** Handle the info page */
void WiFiManager::handleInfo() {
  //DEBUG_WM(F("Info"));

  String page = FPSTR(HTTP_HEAD);
  page.replace("{v}", "Informacoes");
  page += FPSTR(HTTP_SCRIPT);
  page += FPSTR(HTTP_STYLE);
  page += _customHeadElement;
  page += FPSTR(HTTP_HEAD_END);
  page += F("<dl>");
  page += F("<dt>Chip ID</dt><dd>");
  page += ESP.getChipId();
  page += F("</dd>");
  page += F("<dt>Flash Chip ID</dt><dd>");
  page += ESP.getFlashChipId();
  page += F("</dd>");
  page += F("<dt>IDE Flash Size</dt><dd>");
  page += ESP.getFlashChipSize();
  page += F(" bytes</dd>");
  page += F("<dt>Real Flash Size</dt><dd>");
  page += ESP.getFlashChipRealSize();
  page += F(" bytes</dd>");
  page += F("<dt>Soft AP IP</dt><dd>");
  page += WiFi.softAPIP().toString();
  page += F("</dd>");
  page += F("<dt>Soft AP MAC</dt><dd>");
  page += WiFi.softAPmacAddress();
  page += F("</dd>");
  page += F("<dt>Station MAC</dt><dd>");
  page += WiFi.macAddress();
  page += F("</dd>");
  page += F("</dl>");
  page += FPSTR(HTTP_END);

  server->sendHeader("Content-Length", String(page.length()));
  server->send(200, "text/html", page);

  //DEBUG_WM(F("Sent info page"));
}

/** Handle the reset page */
void WiFiManager::handleReset() {
  //DEBUG_WM(F("Reset"));

  String page = FPSTR(HTTP_HEAD);
  page.replace("{v}", ".:Reset EM:.");
  page += FPSTR(HTTP_SCRIPT);
  page += FPSTR(HTTP_STYLE);
  page += _customHeadElement;
  page += FPSTR(HTTP_HEAD_END);
  page += F("Reiniciando EM dentro de alguns instantes...");
  page += FPSTR(HTTP_END);

  server->sendHeader("Content-Length", String(page.length()));
  server->send(200, "text/html", page);

  //DEBUG_WM(F("Sent reset page"));
  delay(5000);
  ESP.reset();
  delay(2000);
}

/** Handle the wifi delete all page */
void WiFiManager::handleWifiDelete() {
  //DEBUG_WM(F("Wifi Delete All"));
  WiFi.disconnect(true);
  String page = FPSTR(HTTP_HEAD);
  page.replace("{v}", ".:Excluindo redes:.");
  page += FPSTR(HTTP_SCRIPT);
  page += FPSTR(HTTP_STYLE);
  page += _customHeadElement;
  page += FPSTR(HTTP_HEAD_END);
  page += F("Excluindo redes registradas anteriormente...");
  page += F("Aguarde.");
  page += FPSTR(HTTP_END);

  server->sendHeader("Content-Length", String(page.length()));
  server->send(200, "text/html", page);
  delay(2000);
  handleWifi(true);
}

void WiFiManager::handleNotFound() {
  if (captivePortal()) { // If captive portal redirect instead of displaying the error page.
    return;
  }
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server->uri();
  message += "\nMethod: ";
  message += ( server->method() == HTTP_GET ) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server->args();
  message += "\n";

  for ( uint8_t i = 0; i < server->args(); i++ ) {
    message += " " + server->argName ( i ) + ": " + server->arg ( i ) + "\n";
  }
  server->sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  server->sendHeader("Pragma", "no-cache");
  server->sendHeader("Expires", "-1");
  server->sendHeader("Content-Length", String(message.length()));
  server->send ( 404, "text/plain", message );
}


/** Redirect to captive portal if we got a request for another domain. Return true in that case so the page handler do not try to handle the request again. */
boolean WiFiManager::captivePortal() {
  if (!isIp(server->hostHeader()) ) {
    //DEBUG_WM(F("Request redirected to captive portal"));
    server->sendHeader("Location", String("http://") + toStringIp(server->client().localIP()), true);
    server->send ( 302, "text/plain", ""); // Empty content inhibits Content-length header so we have to close the socket ourselves.
    server->client().stop(); // Stop is needed because we sent no content length
    return true;
  }
  return false;
}

//start up config portal callback
void WiFiManager::setAPCallback( void (*func)(WiFiManager* myWiFiManager) ) {
  _apcallback = func;
}

//start up save config callback
void WiFiManager::setSaveConfigCallback( void (*func)(void) ) {
  _savecallback = func;
}

//sets a custom element to add to head, like a new style tag
void WiFiManager::setCustomHeadElement(const char* element) {
  _customHeadElement = element;
}

//if this is true, remove duplicated Access Points - defaut true
void WiFiManager::setRemoveDuplicateAPs(boolean removeDuplicates) {
  _removeDuplicateAPs = removeDuplicates;
}



template <typename Generic>
void WiFiManager::DEBUG_WM(Generic text) {
  if (_debug) {
    //Serial.print("*WM: ");
    //Serial.println(text);
  }
}

int WiFiManager::getRSSIasQuality(int RSSI) {
  int quality = 0;

  if (RSSI <= -100) {
    quality = 0;
  } else if (RSSI >= -50) {
    quality = 100;
  } else {
    quality = 2 * (RSSI + 100);
  }
  return quality;
}

/** Is this an IP? */
boolean WiFiManager::isIp(String str) {
  for (int i = 0; i < str.length(); i++) {
    int c = str.charAt(i);
    if (c != '.' && (c < '0' || c > '9')) {
      return false;
    }
  }
  return true;
}

/** IP to String? */
String WiFiManager::toStringIp(IPAddress ip) {
  String res = "";
  for (int i = 0; i < 3; i++) {
    res += String((ip >> (8 * i)) & 0xFF) + ".";
  }
  res += String(((ip >> 8 * 3)) & 0xFF);
  return res;
}
