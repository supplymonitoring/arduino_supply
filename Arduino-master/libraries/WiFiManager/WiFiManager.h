﻿/**************************************************************
   WiFiManager is a library for the ESP8266/Arduino platform
   (https://github.com/esp8266/Arduino) to enable easy
   configuration and reconfiguration of WiFi credentials using a Captive Portal
   inspired by:
   http://www.esp8266.com/viewtopic.php?f=29&t=2520
   https://github.com/chriscook8/esp-arduino-apboot
   https://github.com/esp8266/Arduino/tree/master/libraries/DNSServer/examples/CaptivePortalAdvanced
   Built by AlexT https://github.com/tzapu
   Licensed under MIT license
 **************************************************************/

#ifndef WiFiManager_h
#define WiFiManager_h

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <DNSServer.h>
#include <memory>


extern "C" {
#include "user_interface.h"
}


const char HTTP_HEAD[] PROGMEM                    = "<!DOCTYPE html><html lang=\"en\"><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\"/><title>{v}</title>";
const char HTTP_STYLE[] PROGMEM                   = "<style>.c{text-align: center;} div,input{padding:5px;font-size:1em;} input{width:95%;} body{text-align: center;font-family:verdana;} button{border:0;border-radius:0.3rem;background-color:#1fa3ec;color:#fff;line-height:2.4rem;font-size:1.2rem;width:100%;} .q{float: right;width: 64px;text-align: right;} .l{background: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAALVBMVEX///8EBwfBwsLw8PAzNjaCg4NTVVUjJiZDRUUUFxdiZGSho6OSk5Pg4eFydHTCjaf3AAAAZElEQVQ4je2NSw7AIAhEBamKn97/uMXEGBvozkWb9C2Zx4xzWykBhFAeYp9gkLyZE0zIMno9n4g19hmdY39scwqVkOXaxph0ZCXQcqxSpgQpONa59wkRDOL93eAXvimwlbPbwwVAegLS1HGfZAAAAABJRU5ErkJggg==\") no-repeat left center;background-size: 1em;}</style>";
const char HTTP_SCRIPT[] PROGMEM                  = "<script>function c(l){document.getElementById('s').value=l.innerText||l.textContent;document.getElementById('p').focus();}</script>";
const char HTTP_HEAD_END[] PROGMEM        				= "</head><body><div style='text-align:left;display:inline-block;min-width:260px;'>";
const char HTTP_PORTAL_OPTIONS[] PROGMEM  				= "<form action=\"/wifi\" method=\"get\"><button>Buscar redes Wi-Fi</button></form><br/><form action=\"/wifi-delete\" method=\"get\"><button>Excluir redes salvas</button></form><br/><form action=\"/ajustes\" method=\"get\"><button>Ajustes</button></form><br/><form action=\"/sensores\" method=\"get\"><button>Sensores</button></form><br/><form action=\"/tcs\" method=\"get\"><button>TC's</button></form><br/><form action=\"/i\" method=\"get\"><button>Informacoes</button></form><br/><form action=\"/r\" method=\"post\"><button>Reset EM</button></form>";
const char HTTP_ITEM[] PROGMEM            				= "<div><a href='#p' onclick='c(this)'>{v}</a>&nbsp;<span class='q {i}'>{r}%</span></div>";
const char HTTP_FORM_START[] PROGMEM      				= "<form method='get' action='wifisave'><input id='s' name='s' length=32 placeholder='SSID'><br/><input id='p' name='p' length=64 type='password' placeholder='senha'><br/>";
const char HTTP_FORM_CONFIG_PHONES[] PROGMEM    	= "<form method='get' action='em-settings'><input id='cel_cliente1' name='cel_cliente1' length=13 placeholder='Cel Cliente 01: '><br/><input id='cel_cliente2' name='cel_cliente2' length=13 placeholder='Cel Cliente 02: '><br/><input id='cel_cliente3' name='cel_cliente3' length=13 placeholder='Cel Cliente 03: '><br/>";

const char HTTP_FORM_CONFIG_SI[] PROGMEM 				  = "<form method='get' action='em-settings'><input id='temp_min_si' name='temp_min_si' length=3 placeholder='SI - Temp Min'><input id='temp_max_si' name='temp_max_si' length=3 placeholder='SI - Temp Max'><input id='temp_atual_si' name='temp_atual_si' length=3 placeholder='SI - Temp Atual'><input id='umid_min_si' name='umid_min_si' length=3 placeholder='SI - Umid Min'><input id='umid_max_si' name='umid_max_si' length=3 placeholder='SI - Umid Max'><input id='umid_atual_si' name='umid_atual_si' length=3 placeholder='SI - Umid Atual'><br/>";
const char HTTP_FORM_CONFIG_SE1[] PROGMEM 				= "<form method='get' action='em-settings'><input id='temp_min_se1' name='temp_min_se1' length=3 placeholder='SE1 - Temp Min'><input id='temp_max_se1' name='temp_max_se1' length=3 placeholder='SE1 - Temp Max'><input id='temp_se1_cal_aferida' name='temp_se1_cal_aferida' length=3 placeholder='SE1 - Temp Atual'><br/>";
const char HTTP_FORM_CONFIG_SE2[] PROGMEM 				= "<form method='get' action='em-settings'><input id='temp_min_se2' name='temp_min_se2' length=3 placeholder='SE2 - Temp Min'><input id='temp_max_se2' name='temp_max_se2' length=3 placeholder='SE2 - Temp Max'><input id='temp_se2_cal_aferida' name='temp_se2_cal_aferida' length=3 placeholder='SE2 - Temp Atual'><br/>";
const char HTTP_FORM_CONFIG_TC1[] PROGMEM 				= "<form method='get' action='em-settings'><input id='fase_R_Min' name='fase_R_Min' length=6 placeholder='TC1 Min'><input id='fase_R_Max' name='fase_R_Max' length=6 placeholder='TC1 Max'><input id='fase_R_Atual' name='fase_R_Atual' length=6 placeholder='TC1 Atual'><br/>";
const char HTTP_FORM_CONFIG_TC2[] PROGMEM 				= "<form method='get' action='em-settings'><input id='fase_S_Min' name='fase_S_Min' length=6 placeholder='TC2 Min'><input id='fase_S_Max' name='fase_S_Max' length=6 placeholder='TC2 Max'><input id='fase_S_Atual' name='fase_S_Atual' length=6 placeholder='TC2 Atual'><br/>";
const char HTTP_FORM_CONFIG_TC3[] PROGMEM 				= "<form method='get' action='em-settings'><input id='fase_T_Min' name='fase_T_Min' length=6 placeholder='TC3 Min'><input id='fase_T_Max' name='fase_T_Max' length=6 placeholder='TC3 Max'><input id='fase_T_Atual' name='fase_T_Atual' length=6 placeholder='TC3 Atual'><br/>";
const char HTTP_FORM_PARAM[] PROGMEM      				= "<br/><input id='{i}' name='{n}' maxlength={l} placeholder='{p}' value='{v}' {c}>";
const char HTTP_FORM_END[] PROGMEM        				= "<br/><button type='submit'>Salvar</button></form>";
const char HTTP_SCAN_LINK[] PROGMEM       				= "<br/><div class=\"c\"><a href=\"/wifi\">Buscar</a></div>";
const char HTTP_FORM_BACK[] PROGMEM       				= "<br/><div class=\"c\"><a href=\"/\">Menu</a></div>";
const char HTTP_SAVED[] PROGMEM           				= "<div>Configuracoes realizadas!<br/>Tentando conectar a rede...<br/></div>";
const char HTTP_SAVED2[] PROGMEM           				= "<div>Configuracoes realizadas!<br/></div>";
const char HTTP_SAVED3[] PROGMEM           				= "<div>Mensagens salvas<br/></div>";
const char HTTP_END[] PROGMEM             				= "</div></body></html>";

#define WIFI_MANAGER_MAX_PARAMS 10

class WiFiManagerParameter {
  public:
    WiFiManagerParameter(const char *custom);
    WiFiManagerParameter(const char *id, const char *placeholder, const char *defaultValue, int length);
    WiFiManagerParameter(const char *id, const char *placeholder, const char *defaultValue, int length, const char *custom);

    const char *getID();
    const char *getValue();
    const char *getPlaceholder();
    int         getValueLength();
    const char *getCustomHTML();
  private:
    const char *_id;
    const char *_placeholder;
    char       *_value;
    int         _length;
    const char *_customHTML;

    void init(const char *id, const char *placeholder, const char *defaultValue, int length, const char *custom);

    friend class WiFiManager;
};


class WiFiManager
{
  public:
    WiFiManager();

    boolean       autoConnect();
    boolean       autoConnect(char const *apName, char const *apPassword = NULL);

    //if you want to always start the config portal, without trying to connect first
    boolean       startConfigPortal();
    boolean       startConfigPortal(char const *apName, char const *apPassword = NULL);

    // get the AP name of the config portal, so it can be used in the callback
    String        getConfigPortalSSID();

    ///////////////////////////////////////
    //FUNCOES PROPRIAS
    String 		  getCelClientes();
    String 		  getSIParams();
    String 		  getSEParams();
    String 		  getTCParams();
    void        resetSettings();   
    void        setCelClientes(String cel1, String cel2, String cel3);
    void        setSIParams(String siTempMin, String siTempMax, String siTemp_Atual_Aferida,String ktSI, String siUmidMin, String siUmidMax, String siUmid_Atual_Aferida, String kuSI);
    void 	  	  setSEParams(String temp_min_s1, String temp_max_s1, String temp_cal_aferida_se1, String temp_min_s2, String temp_max_s2, String temp_cal_aferida_se2);
    void 		    setTCParams(String faseR_cMin, String faseR_cMax, String faseR_cal_aferida, String faseS_cMin, String faseS_cMax, String faseS_cal_aferida, String faseT_cMin, String faseT_cMax, String faseT_cal_aferida);
    //////////////////////////////////////

    //sets timeout before webserver loop ends and exits even if there has been no setup.
    //useful for devices that failed to connect at some point and got stuck in a webserver loop
    //in seconds setConfigPortalTimeout is a new name for setTimeout
    void          setConfigPortalTimeout(unsigned long seconds);
    void          setTimeout(unsigned long seconds);

    //sets timeout for which to attempt connecting, useful if you get a lot of failed connects
    void          setConnectTimeout(unsigned long seconds);


    void          setDebugOutput(boolean debug);
    //defaults to not showing anything under 8% signal quality if called
    void          setMinimumSignalQuality(int quality = 8);
    //sets a custom ip /gateway /subnet configuration
    void          setAPStaticIPConfig(IPAddress ip, IPAddress gw, IPAddress sn);
    //sets config for a static IP
    void          setSTAStaticIPConfig(IPAddress ip, IPAddress gw, IPAddress sn);
    //called when AP mode and config portal is started
    void          setAPCallback( void (*func)(WiFiManager*) );
    //called when settings have been changed and connection was successful
    void          setSaveConfigCallback( void (*func)(void) );
    //adds a custom parameter
    void          addParameter(WiFiManagerParameter *p);
    //if this is set, it will exit after config, even if connection is unsuccessful.
    void          setBreakAfterConfig(boolean shouldBreak);
    //if this is set, try WPS setup when starting (this will delay config portal for up to 2 mins)
    //TODO
    //if this is set, customise style
    void          setCustomHeadElement(const char* element);
    //if this is true, remove duplicated Access Points - defaut true
    void          setRemoveDuplicateAPs(boolean removeDuplicates);

  private:
    std::unique_ptr<DNSServer>        dnsServer;
    std::unique_ptr<ESP8266WebServer> server;

    //const int     WM_DONE                 = 0;
    //const int     WM_WAIT                 = 10;

    //const String  HTTP_HEAD = "<!DOCTYPE html><html lang=\"en\"><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/><title>{v}</title>";

    void          setupConfigPortal();
    void          startWPS();

    const char*   _apName                 = "no-net";
    const char*   _apPassword             = NULL;
    String        _ssid                   = "";
    String        _pass                   = "";
    unsigned long _configPortalTimeout    = 0;
    unsigned long _connectTimeout         = 0;
    unsigned long _configPortalStart      = 0;

    //VARIAVEIS PROPRIAS
    String        _celClientes            = "";
    String        _celCliente1            = "";
    String        _celCliente2            = "";
    String        _celCliente3            = "";
    String		  _tempSIMin 			  = "";
    String		  _tempSIMax 			  = "";
    String		  _tempSIAtual 			  = "";
    String      _ktSI             = "";
    String		  _umidSIMin 			  = "";
    String		  _umidSIMax 			  = "";
    String		  _umidSIAtual 			  = "";
    String      _kuSI             = "";
    String 		  _tempSE1Min		 	  = "";
    String 		  _tempSE1Max	 		  = "";
    String		  _tempSE1_Atual		  = "";
    String 		  _tempSE2Min		 	  = "";
    String 		  _tempSE2Max	 		  = "";
    String		  _tempSE2_Atual	 	  = "";
    String 		  _faseRmin 			  = "";
    String 		  _faseRmax 			  = "";
    String 		  _faseR_calaferida 	  = "";
    String 		  _faseSmin 			  = "";
    String 		  _faseSmax 			  = "";
    String 		  _faseS_calaferida		  = "";
    String 		  _faseTmin 			  = "";
    String 		  _faseTmax 			  = "";
    String 		  _faseT_calaferida		  = "";
    
    //////

    IPAddress     _ap_static_ip;
    IPAddress     _ap_static_gw;
    IPAddress     _ap_static_sn;
    IPAddress     _sta_static_ip;
    IPAddress     _sta_static_gw;
    IPAddress     _sta_static_sn;

    int           _paramsCount            = 0;
    int           _minimumQuality         = -1;
    boolean       _removeDuplicateAPs     = true;
    boolean       _shouldBreakAfterConfig = false;
    boolean       _tryWPS                 = false;

    const char*   _customHeadElement      = "";

    //String        getEEPROMString(int start, int len);
    //void          setEEPROMString(int start, int len, String string);

    int           status = WL_IDLE_STATUS;
    int           connectWifi(String ssid, String pass);
    uint8_t       waitForConnectResult();

    void          handleRoot();
    void          handleWifi(boolean scan);
    void          handleWifiSave();
    void          handleWifiDelete();
    void          handleInfo();
    void		      handleAjustes();
    void          handleSensores();
    void          handleTCS();
    void 		      handleMensagens();
    void 		      handleMensagensSave();
    void 		      handleAjustesSave();
    void		      handleAjustesAvancados();
    void          handleReset();
    void          handleNotFound();
    void          handle204();
    boolean       captivePortal();
    boolean       configPortalHasTimeout();

    // DNS server
    const byte    DNS_PORT = 53;

    //helpers
    int           getRSSIasQuality(int RSSI);
    boolean       isIp(String str);
    String        toStringIp(IPAddress ip);

    boolean       connect;
    boolean       _debug = true;

    void (*_apcallback)(WiFiManager*) = NULL;
    void (*_savecallback)(void) = NULL;

    WiFiManagerParameter* _params[WIFI_MANAGER_MAX_PARAMS];

    template <typename Generic>
    void          DEBUG_WM(Generic text);

    template <class T>
    auto optionalIPFromString(T *obj, const char *s) -> decltype(  obj->fromString(s)  ) {
      return  obj->fromString(s);
    }
    auto optionalIPFromString(...) -> bool {
      DEBUG_WM("NO fromString METHOD ON IPAddress, you need ESP8266 core 2.1.0 or newer for Custom IP configuration to work.");
      return false;
    }
};

#endif
